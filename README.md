## Instructions :

1. Clone the repository :
>  git clone git@gitlab.com:Pstanislas/cloudprojectpedebearnstanislas.git
2. Start docker-compose :
>  docker-compose up
3. Wait severals minutes
4. When everything is finished, the application is automatically open in a Firefox's windows.

## Informations :

There are 3 different dockers :

- postgres Docker : Provide a functional DB. DB is automatically created with a sql file.
- glassfish Docker : Provide a functional glassfish server and deploy web services. Because glasfish is actually prohibited with docker, I have created a specific dockerfile to create the connection between the Server and the DB and make other parameters. You can found it in the "ws" folder.
- client Docker : I created this docker with a dockerfile to open automatically my webService in a Firefox page.

## About the webApp :

A simple CRUD application to manage differents cooking recipes. Actually, it's a Beta version and the visual aspect is the most simple as possible. But the CRUD aspect works.