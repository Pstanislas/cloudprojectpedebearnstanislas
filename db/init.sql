GRANT ALL PRIVILEGES ON DATABASE docker TO docker;



create table Recette(
  numero_recette varchar(10),
  nom_recette varchar (50),
  script_recette varchar(1000),
  constraint Recette_key primary key (numero_recette));

create table Produit(
  numero_produit varchar(10),
  libelle_produit varchar(50),
  categorie varchar(50),
  constraint Produit_key primary key (numero_produit));

create table Utilisateur(
  numero_utilisateur varchar(10),
  nom varchar(20),
  prenom varchar (20),
  constraint Utilisateur_key primary key (numero_utilisateur));

create table Ingredient(
  numero_recette varchar(10),
  numero_produit varchar(10),
  quantite_recette varchar(10),
  constraint Ingredient_key primary key (numero_recette, numero_produit),
  constraint Ingredient_Recette_foreign_key foreign key (numero_recette) references Recette(numero_recette),
  constraint Ingredient_Produit_foreign_key foreign key (numero_produit) references Produit(numero_produit));

create table Stock(
  numero_utilisateur varchar(10),
  numero_produit varchar(10),
  quantite_stock varchar(10),
  constraint Stock_key primary key (numero_utilisateur, numero_produit),
  constraint Stock_utilisateur_foreign_key foreign key (numero_utilisateur) references Utilisateur(numero_utilisateur),
  constraint Stock_produit_foreign_key foreign key (numero_produit) references Produit(numero_produit));

insert into Recette (numero_recette, nom_recette, script_recette) values ('R1','quiche lorraine', 'Etape 1 : Préchauffer le four à 180°C (thermostat 6). Etaler la pâte dans un moule. Etape 2 : la piquer à la fourchette. Parsemer de copeaux de beurre. Etape 3 : Faire rissoler les lardons à la poêle. Etape 4 : Battre les oeufs, la crème fraîche et le lait. Etape 5 : Ajouter les lardons. Etape 6 : Assaisonner de sel, de poivre et de muscade. Etape 7 : Verser sur la pâte. Etape 8 : Cuire 45 à 50 min. Etape 9 : C''est prêt.');
insert into Recette (numero_recette, nom_recette, script_recette) values ('R2','gâteau au chocolat', 'Etape 1 : Préchauffez votre four à 180°C (thermostat 6). Dans une casserole, faites fondre le chocolat et le beurre coupé en morceaux à feu très doux. Etape 2 : Dans un saladier, ajoutez le sucre, les oeufs, la farine. Mélangez. Etape 3 : Ajoutez le mélange chocolat/beurre. Mélangez bien. Etape 4 : Beurrez et farinez votre moule puis y versez la pâte à gâteau. Etape 5 : Faites cuire au four environ 20 minutes. Etape 6 : A la sortie du four le gâteau ne paraît pas assez cuit. C''est normal, laissez-le refroidir puis démoulez- le.');

insert into Produit (numero_produit, libelle_produit, categorie) values ('P1', 'pâte brisée', 'traiteur');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P2', 'lardon', 'charcuterie');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P3', 'beurre', 'beurre et crème');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P4', 'crème fraiche', 'beurres et crèmes');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P5', 'oeuf', 'laits et oeufs');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P6', 'lait', 'laits et oeufs');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P7', 'chocolat à pâtisser noir', 'chocolats et confiseries');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P8', 'farine', 'desserts, sucres et farines');
insert into Produit (numero_produit, libelle_produit, categorie) values ('P9', 'sucre en poudre', 'desserts, sucres et farines');

insert into Utilisateur values ('U1', 'Bigot', 'Aymeric');
insert into Utilisateur values ('U2', 'Pédebéarn', 'Stanislas');

insert into Ingredient values ('R1', 'P1', '200 g');
insert into Ingredient values ('R1', 'P2', '200 g');
insert into Ingredient values ('R1', 'P3', '30 g');
insert into Ingredient values ('R1', 'P4', '3');
insert into Ingredient values ('R1', 'P5', '20 cl');
insert into Ingredient values ('R1', 'P6', '20 cl');
insert into Ingredient values ('R2', 'P7', '200 g');
insert into Ingredient values ('R2', 'P3', '100 g');
insert into Ingredient values ('R2', 'P5', '3');
insert into Ingredient values ('R2', 'P8', '50');
insert into Ingredient values ('R2', 'P9', '100');

insert into Stock values ('U1', 'P1', '200 g');
insert into Stock values ('U1', 'P3', '200 g');
insert into Stock values ('U1', 'P6', '50 cl ');
insert into Stock values ('U1', 'P8', '100g');
insert into Stock values ('U2', 'P4', '40g');
insert into Stock values ('U2', 'P7', '300g');
